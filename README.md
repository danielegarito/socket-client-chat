# README #

This repository is a test of connection to the `@cognigy/socket-client` socket client

### Code ###

* React
* Redux
* Thunk
* [`@cognigy/socket-client`](https://github.com/Cognigy/SocketClient)
* Redux middlewares

### Setup ###

- `git clone` of the repo
- `cd ~/YOUR_PATH_TO/socket-client-chat`
- `npm install` to install all the dependencies
- `create .env file` and add inside the two variables `SOCKET_UR`, `SOCKET_TOKEN`
- `npm start` run the application
