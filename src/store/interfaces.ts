import { IProcessOutputData, IProcessReplyPayload } from "@cognigy/socket-client/lib/interfaces/output"
import { Input } from "@cognigy/socket-client/lib/interfaces/input"

export interface IMessage extends IProcessOutputData {}
export interface IOutputError extends IProcessReplyPayload {}
export interface ISendMessageData extends Input {}

export interface IMessages {
  data: IMessage[]
  error: null | Error
  loading: boolean
}

export interface IState {
  messages: IMessages
  socket: ISocket
}

export interface ISocket {
  connected: boolean
  reconnect?: boolean
  loading: boolean
  error: any
}
