import { applyMiddleware, compose, createStore } from "redux"
import { logger } from "redux-logger"
import thunk from "redux-thunk"
import reducers from "./reducers"
import Client from "../services/websocket.client"
import { storageMiddleware } from "./middleware/storageMiddelware"
import { socketListenerMiddleware } from "./middleware/socketListenerMiddelware"
import { IState } from "./interfaces"

const composeEnhancer: typeof compose = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const client = new Client(process.env.SOCKET_URL, process.env.SOCKET_TOKEN, { forceWebsockets: true })

const socketInstance = client.getInstance()

const stateFromStorage: string | null = localStorage.getItem("store")
const parsedState: IState = stateFromStorage ? JSON.parse(stateFromStorage) : {}
const preloadState = { ...parsedState }

export const store = createStore(
  reducers,
  preloadState,
  composeEnhancer(
    applyMiddleware(
      thunk.withExtraArgument(socketInstance),
      logger,
      storageMiddleware,
      socketListenerMiddleware
    )
  )
)
