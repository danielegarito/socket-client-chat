import moment from "moment"
import { SocketClient } from "@cognigy/socket-client"
import { Dispatch } from "react"
import { DATE_FORMAT } from "../../utils/costants"
import { IMessage, IOutputError, ISendMessageData } from "../interfaces"
import {onConnectSocketError} from "./socket";

export const BEFORE_SEND_MESSAGE = "BEFORE_SEND_MESSAGE"
export const SEND_MESSAGE_SUCCESS = "SEND_MESSAGE_SUCCESS"
export const SEND_MESSAGE_ERROR = "SEND_MESSAGE_ERROR"

export const BEFORE_RECEIVE_MESSAGE = "BEFORE_RECEIVE_MESSAGE"
export const RECEIVE_MESSAGE_SUCCESS = "RECEIVE_MESSAGE_SUCCESS"
export const RECEIVE_MESSAGE_ERROR = "RECEIVE_MESSAGE_ERROR"

const onBeforeSendMessage = () => {
  return { type: BEFORE_SEND_MESSAGE }
}

const onSendMessageSuccess = (payload: IMessage) => {
  return {
    type: SEND_MESSAGE_SUCCESS,
    payload
  }
}

const onSendMessageError = (error: any) => {
  return {
    type: SEND_MESSAGE_ERROR,
    error
  }
}

const onBeforeReceiveMessage = () => {
  return { type: BEFORE_RECEIVE_MESSAGE }
}

const onReceiveMessageSuccess = (payload: IMessage) => {
  return {
    type: RECEIVE_MESSAGE_SUCCESS,
    payload
  }
}

const onReceiveMessageError = (error: IOutputError) => {
  return {
    type: RECEIVE_MESSAGE_ERROR,
    error
  }
}

export const sendMessage = ({ text, data }: ISendMessageData) =>
  (dispatch: Dispatch<any>, getState: any, socketClient: SocketClient) => {
    dispatch(onBeforeSendMessage())
    try {
      socketClient.sendMessage(text, data)
      const payload = {
        date: moment().format(DATE_FORMAT),
        text,
        data
      }
      return dispatch(onSendMessageSuccess(payload))
    } catch (error: any) {
      dispatch(onSendMessageError(error))
    }
  }

export const receiveMessage = () => (dispatch: Dispatch<any>, getState: any, socketClient: SocketClient) => {
  dispatch(onBeforeReceiveMessage())
  const { socket: { connected } } = getState()
  if(!connected) {
    dispatch(onConnectSocketError({
      error: "[SocketClient] Unable to directly send your message since we are not connected. Your message will be buffered and sent later on.",
      reconnect: true
    }))
  }

  socketClient.on("error", (error: IOutputError) => dispatch(onReceiveMessageError(error)))
  socketClient.on("output", (output: IMessage) => {
    const payload = {
      date: moment().format(DATE_FORMAT),
      ...output
    }
    dispatch(onReceiveMessageSuccess(payload))
  })
}
