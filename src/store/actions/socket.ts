import { SocketClient } from "@cognigy/socket-client"
import { Dispatch } from "react"

export const BEFORE_DISCONNECT_CLIENT_SOCKET = "BEFORE_DISCONNECT_CLIENT_SOCKET"
export const DISCONNECT_CLIENT_SOCKET_SUCCESS = "DISCONNECT_CLIENT_SOCKET_SUCCESS"
export const BEFORE_CONNECT_CLIENT_SOCKET = "BEFORE_CONNECT_CLIENT_SOCKET"
export const CONNECT_CLIENT_SOCKET_ERROR = "CONNECT_CLIENT_SOCKET_ERROR"
export const CONNECT_CLIENT_SOCKET_TIMEOUT = "CONNECT_CLIENT_SOCKET_TIMEOUT"
export const CONNECT_CLIENT_SOCKET_SUCCESS = "CONNECT_CLIENT_SOCKET_SUCCESS"

const onBeforeDisconnectSocket = () => {
  return { type: BEFORE_DISCONNECT_CLIENT_SOCKET }
}

const onDisconnectSocketSuccess = () => {
  return { type: DISCONNECT_CLIENT_SOCKET_SUCCESS }
}

const onBeforeConnectSocket = () => {
  return { type: BEFORE_CONNECT_CLIENT_SOCKET }
}

const onConnectSocketSuccess = () => {
  return { type: CONNECT_CLIENT_SOCKET_SUCCESS }
}

export const onConnectSocketError = (error: any = null) => {
  return { type: CONNECT_CLIENT_SOCKET_ERROR, payload: { error } }
}

const onConnectSocketTimeout = (error: any = null) => {
  return { type: CONNECT_CLIENT_SOCKET_TIMEOUT, payload: { error } }
}

export const connectSocket = () => async (dispatch: Dispatch<any>, getState: any, socketClient: SocketClient) => {
    dispatch(onBeforeConnectSocket())
    try {
      await socketClient.connect()
      socketClient.on("connect_error", () => dispatch(onConnectSocketError()))
      socketClient.on("connect_timeout", () => dispatch(onConnectSocketTimeout()))

      return dispatch(onConnectSocketSuccess())
    } catch (error: any) {
      dispatch(onConnectSocketError(error))
    }
  }

export const disconnectSocket = () => (dispatch: Dispatch<any>, getState: any, socketClient: SocketClient) => {
    dispatch(onBeforeDisconnectSocket())
    socketClient.disconnect()

    return dispatch(onDisconnectSocketSuccess())
  }
