import {
  BEFORE_RECEIVE_MESSAGE,
  BEFORE_SEND_MESSAGE,
  RECEIVE_MESSAGE_ERROR,
  RECEIVE_MESSAGE_SUCCESS,
  SEND_MESSAGE_ERROR,
  SEND_MESSAGE_SUCCESS
} from "../actions/messages"
import { IMessages } from "../interfaces"

const initialState: IMessages = {
  data: [],
  error: null,
  loading: false
}
export const messagesReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case BEFORE_SEND_MESSAGE:
      return {
        ...state,
        loading: true
      }
    case SEND_MESSAGE_SUCCESS:
      return {
        ...state,
        data: [...state.data, { ...action.payload }],
        loading: false
      }
    case SEND_MESSAGE_ERROR:
      return {
        ...state,
        error: action.payload,
        loading: false
      }
    case BEFORE_RECEIVE_MESSAGE:
      return {
        ...state,
        loading: true
      }
    case RECEIVE_MESSAGE_SUCCESS:
      return {
        ...state,
        data: [...state.data, { ...action.payload }],
        loading: false
      }
    case RECEIVE_MESSAGE_ERROR:
      return {
        ...state,
        error: action.payload,
        loading: false
      }
    default:
      return state
  }
}
