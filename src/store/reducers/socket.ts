import {
  BEFORE_CONNECT_CLIENT_SOCKET,
  BEFORE_DISCONNECT_CLIENT_SOCKET,
  CONNECT_CLIENT_SOCKET_ERROR,
  CONNECT_CLIENT_SOCKET_SUCCESS,
  DISCONNECT_CLIENT_SOCKET_SUCCESS
} from "../actions/socket"
import { ISocket } from "../interfaces"

const initialState: ISocket = {
  connected: false,
  reconnect: false,
  loading: false,
  error: null
}
export const socketReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case BEFORE_CONNECT_CLIENT_SOCKET:
      return {
        ...state,
        loading: true
      }
    case CONNECT_CLIENT_SOCKET_SUCCESS:
      return {
        ...state,
        connected: true,
        loading: false
      }
    case BEFORE_DISCONNECT_CLIENT_SOCKET:
      return {
        ...state,
        loading: true
      }
    case DISCONNECT_CLIENT_SOCKET_SUCCESS:
      return {
        ...state,
        connected: false,
        loading: false
      }
    case CONNECT_CLIENT_SOCKET_ERROR:
      return {
        ...state,
        connected: false,
        loading: false,
        ...action.payload
      }
    default:
      return state
  }
}
