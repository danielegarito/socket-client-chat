import * as Redux from "redux"
import { receiveMessage, SEND_MESSAGE_SUCCESS } from "../actions/messages"
import {IState} from "../interfaces";

let listenerRegistered = false

export const socketListenerMiddleware: any = (store: Redux.Store<IState, any>) => {
  return (next: Redux.Dispatch<any>) => {
    return (action: any) => {
      next(action)

      if (listenerRegistered) return null

      if (action.type === SEND_MESSAGE_SUCCESS) {
        const { dispatch } = store
        dispatch(receiveMessage())
        listenerRegistered = true
      }
    }
  }
}
