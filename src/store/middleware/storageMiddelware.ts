import * as Redux from "redux"
import { DISCONNECT_CLIENT_SOCKET_SUCCESS } from "../actions/socket"
import {IState} from "../interfaces"

export const storageMiddleware: any = (store: Redux.Store<IState, any>) => {
  return (next: Redux.Dispatch<any>) => {
    return (action: any) => {
      next(action)
      const state = store.getState()
      localStorage.setItem("store", JSON.stringify(state))

      if (action.type === DISCONNECT_CLIENT_SOCKET_SUCCESS) {
        localStorage.removeItem("store")
      }
    }
  }
}
