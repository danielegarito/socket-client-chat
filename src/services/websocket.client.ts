import { SocketClient } from "@cognigy/socket-client"

let instance: SocketClient

export default class Client {
  private readonly socketUrl: string
  private readonly socketToken: string
  private readonly options: any

  constructor(socketUrl: string = "", socketToken: string = "", options: any = {}) {
    this.socketUrl = socketUrl
    this.socketToken = socketToken
    this.options = options
  }

  public getInstance() {
    if (instance) {
      return instance
    }

    instance = this.getClient()

    return instance
  }

  public getClient() {
    instance = new SocketClient(this.socketUrl, this.socketToken, this.options)

    return instance
  }
}
