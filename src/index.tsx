import ReactDOM from "react-dom"
import React, { Component } from "react"
import ChatScreen from "./components/screens/Chat.screen"
import { Provider } from "react-redux"
import { store } from "./store"

class App extends Component {
  render() {
    return <ChatScreen />
  }
}

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
)
