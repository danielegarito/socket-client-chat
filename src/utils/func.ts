export const getBase64 = (file: File) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.onload = () => resolve(reader.result)
    reader.onerror = (error: ProgressEvent<FileReader>) => reject(error)
    reader.readAsDataURL(file)
  })
}
