import React, { BaseSyntheticEvent, Component } from "react"
import { connect } from "react-redux"
import get from "lodash/get"
import { sendMessage } from "../../store/actions/messages"
import { connectSocket, disconnectSocket } from "../../store/actions/socket"
import { Input } from "@cognigy/socket-client/lib/interfaces/input"
import { getBase64 } from "../../utils/func"
import Chat from "../elements/Chat/Chat.component"
import { IMessages, IState } from "../../store/interfaces"

interface IChatState extends Input {}

interface IChatProps {
  messages: IMessages
  connected: boolean
  loading: boolean
  sendMessage: (message: IChatState) => void
  connectSocket: () => void
  disconnectSocket: () => void
}

class ChatScreen extends Component<IChatProps, IChatState> {
  private readonly initialState: IChatState

  constructor(props: IChatProps) {
    super(props)
    this.state = {
      text: "",
      data: undefined
    }
    this.initialState = { ...this.state }
  }

  public componentDidMount() {
    this.shouldReconnect()
  }

  public render() {
    const { messages, connected, loading } = this.props
    const { text } = this.state
    const items = get(messages, "data")

    return (
      <Chat
        items={items}
        connected={connected}
        loading={loading}
        text={text}
        onConnect={this.handleConnection}
        onKeyPress={this.handleKeyPress}
        onUpload={this.handleUpload}
        onClick={this.handleClick}
        onChange={this.handleChange}
      />
    )
  }

  public shouldReconnect() {
    const { connected, connectSocket} = this.props
    if(!connected) return
    connectSocket()
  }

  public handleConnection = () => {
    const { connected, connectSocket, disconnectSocket } = this.props
    if (!connected) {
      return connectSocket()
    }
    return disconnectSocket()
  }

  public handleMessage = () => {
    const { sendMessage } = this.props
    const { text, data } = this.state

    if (!text && !data) return

    const withDefaultText = !text ? "Look an image!" : text
    sendMessage({ text: withDefaultText, data })
    this.setState(this.initialState)
  }

  public handleClick = (event: React.MouseEvent) => {
    if (event.type !== "click") return

    this.handleMessage()
  }

  public handleKeyPress = (event: React.KeyboardEvent) => {
    if (event.key !== "Enter") return

    this.handleMessage()
  }

  public handleUpload = async (event: BaseSyntheticEvent) => {
    const file = event.target.files[0]

    const base64File = await getBase64(file)
    this.setState({ data: { imgSrc: `${base64File}` } })
    event.target.value = null
  }

  public handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target
    this.setState({ text: value })
  }
}

const mapStateToProps = ({ messages, socket: { connected, loading } }: IState) => {
  return { messages, connected, loading }
}

export default connect(mapStateToProps, { sendMessage, connectSocket, disconnectSocket })(ChatScreen)
