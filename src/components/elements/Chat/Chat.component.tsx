import { ChatWrapper } from "./Chat.style"
import Grid from "@material-ui/core/Grid"
import Header from "../Header/Header.component"
import Divider from "@material-ui/core/Divider"
import ChatList from "../List/List.component"
import InputBar from "../InputBar/InputBar.component"
import React, { BaseSyntheticEvent } from "react"
import { IMessage } from "../../../store/interfaces"

interface IChatProps {
  items: IMessage[]
  connected: boolean
  loading: boolean
  text: string
  onConnect: () => any
  onKeyPress: (event: React.KeyboardEvent) => any
  onUpload: (event: BaseSyntheticEvent) => void
  onClick: (event: React.MouseEvent) => any
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void
}

const getLabels = (connected: boolean): { buttonLabel: string; statusLabel: string } => {
  const buttonLabel = connected ? "Finish" : "Start"
  const statusLabel = connected ? "Online" : "Offline"

  return { buttonLabel, statusLabel }
}

const Chat = (props: IChatProps) => {
  const { connected, loading, text, items, onConnect, onKeyPress, onUpload, onClick, onChange } = props
  const { buttonLabel, statusLabel } = getLabels(connected)

  return (
    <ChatWrapper item xs={11} sm={8} md={6} xl={4}>
      <Grid item>
        <Header
          onConnect={onConnect}
          buttonLabel={buttonLabel}
          statusLabel={statusLabel}
          connected={connected}
        />
        <ChatList items={items} loading={loading} />
        <Divider />
        <InputBar
          onKeyPress={onKeyPress}
          onUpload={onUpload}
          onClick={onClick}
          onChange={onChange}
          value={text}
        />
      </Grid>
    </ChatWrapper>
  )
}

export default Chat
