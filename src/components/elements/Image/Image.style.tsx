import styled from "styled-components"

interface IStyledImage {
  src: string
}

export const StyledImage = styled("img")<IStyledImage>`
  width: 100%;
  height: 220px;
  border-radius: 10px;
  margin-bottom: 5px;
`
