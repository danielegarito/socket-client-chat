import React from "react"
import { StyledImage } from "./Image.style"

interface IImageProps {
  src: string
  text: string | undefined
}

export const Image = (props: IImageProps) => {
  const { src, text } = props

  if (!src) return <></>

  return <StyledImage src={src} alt={text} title={text} />
}
