import Grid from "@material-ui/core/Grid"
import styled from "styled-components"
import List from "@material-ui/core/List"

export const StyledList = styled(List)`
  height: 40vh;
  overflow-y: scroll;
  position: relative;
`

export const StyledGrid = styled(Grid)`
  position: relative;
  overflow: hidden;
  padding: 10px 0;
`
