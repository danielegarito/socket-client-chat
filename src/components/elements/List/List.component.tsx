import React, { useEffect } from "react"
import { StyledGrid, StyledList } from "./List.style"
import { Loader } from "../Loader/Loader.component"
import ChatListItem from "../ListItem/ListItem.component"
import { LOADER_IMAGE_PATH } from "../../../utils/costants"
import { IMessage } from "../../../store/interfaces"

interface IChatListProps {
  items: IMessage[]
  loading: boolean
}

const ChatList = ({ items, loading }: IChatListProps) => {
  if (!items) return <StyledGrid item />

  const messagesEndRef: React.RefObject<HTMLUListElement> = React.useRef(null)
  useEffect(() => {
    messagesEndRef!.current!.scrollTop = messagesEndRef!.current!.scrollHeight
  }, [items])

  const renderListItem = (message: IMessage, index: number) => {
    return <ChatListItem key={index} {...message} />
  }

  return (
    <StyledGrid item>
      <StyledList ref={messagesEndRef}>
        {items.map(renderListItem)}
      </StyledList>
      <Loader loading={loading} src={LOADER_IMAGE_PATH} />
    </StyledGrid>
  )
}

export default ChatList
