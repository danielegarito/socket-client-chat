import styled from "styled-components"
import Badge from "@material-ui/core/Badge"

export const StatusBarWrapper = styled("div")`
  display: flex;
  align-items: center;
`

interface IStyledBadgeProps {
  badgecolor: string
}

export const StyledBadge = styled(Badge)<IStyledBadgeProps>`
  margin: 0 16px 0 6px;
  span {
    width: 12px;
    height: 12px;
    border-radius: 23px;
    background-color: ${({ badgecolor }) => `${badgecolor}`};
  }
`
