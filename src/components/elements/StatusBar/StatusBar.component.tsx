import React from "react"
import { StatusBarWrapper, StyledBadge } from "./StatusBar.style"
import Typography from "@material-ui/core/Typography"

interface IStatusBarProps {
  connected: boolean
  label: string
}

export const StatusBar = (props: IStatusBarProps) => {
  const { connected, label } = props
  const badgeColor = connected ? "green" : "red"

  return (
    <StatusBarWrapper>
      <StyledBadge badgeContent="Connected" variant="dot" badgecolor={badgeColor} />
      <Typography>{label}</Typography>
    </StatusBarWrapper>
  )
}
