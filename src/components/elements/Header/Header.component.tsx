import React from "react"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import { Button, Toolbar } from "@material-ui/core"
import { StyledAppBar } from "./Header.style"
import { StatusBar } from "../StatusBar/StatusBar.component"

interface IHeaderProps {
  buttonLabel: string
  statusLabel: string
  connected: boolean
  onConnect: () => any
}

const Header = (props: IHeaderProps) => {
  const { onConnect, buttonLabel, statusLabel, connected } = props
  return (
    <Grid item xs={12}>
      <StyledAppBar position="static">
        <Toolbar>
          <StatusBar connected={connected} label={statusLabel} />
          <Typography variant="h6">Live chat</Typography>
          <Button onClick={onConnect} color="inherit">
            {buttonLabel}
          </Button>
        </Toolbar>
      </StyledAppBar>
    </Grid>
  )
}

export default Header
