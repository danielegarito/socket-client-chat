import styled from "styled-components"
import AppBar from "@material-ui/core/AppBar"

export const StyledAppBar = styled(AppBar)`
  &.MuiPaper-root {
    background-color: #2b6caf;
    div {
      justify-content: space-between;
    }
  }
`
