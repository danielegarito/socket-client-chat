import React from "react"
import { LoaderImage, StyledLoader } from "./Loader.style"

interface ILoaderProps {
  loading: boolean
  src: string
}

export const Loader = ({ loading, src }: ILoaderProps) => {
  if (!loading) return <></>

  return (
    <StyledLoader>
      <LoaderImage src={src} alt="Loading..." title="Loading..." />
    </StyledLoader>
  )
}
