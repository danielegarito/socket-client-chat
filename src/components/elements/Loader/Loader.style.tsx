import styled from "styled-components"
import Grid from "@material-ui/core/Grid"

export const StyledLoader = styled(Grid)`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #fff;
`

export const LoaderImage = styled("img")`
  width: 120px;
`
