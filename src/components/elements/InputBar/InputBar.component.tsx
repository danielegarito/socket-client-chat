import React from "react"
import FileUploader from "./FileUploader.component"
import { InputBarContainer, InputsWrapper, StyledButton, StyledTextFiled } from "./InputBar.style"

interface IChatButtonProps {
  onClick: (event: React.MouseEvent) => void
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void
  onUpload: (event: React.ChangeEvent<HTMLInputElement>) => void
  onKeyPress: (event: React.KeyboardEvent) => void
  value: string
}

const InputBar = (props: IChatButtonProps) => {
  const { onClick, onChange, onKeyPress, onUpload, value } = props
  return (
    <InputBarContainer container style={{ padding: "15px" }}>
      <InputsWrapper>
        <StyledTextFiled
          label="Type Something"
          color="primary"
          id="outlined-name"
          value={value}
          onChange={onChange}
          variant="outlined"
          onKeyPress={onKeyPress}
        />
        <FileUploader onUpload={onUpload} />
      </InputsWrapper>
      <StyledButton onClick={onClick} variant="contained">
        Send
      </StyledButton>
    </InputBarContainer>
  )
}

export default InputBar
