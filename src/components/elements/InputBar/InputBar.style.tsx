import styled from "styled-components"
import TextField from "@material-ui/core/TextField"
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"

export const InputBarContainer = styled(Grid)`
  justify-content: space-between;
`

export const InputsWrapper = styled(Grid)`
  position: relative;
  flex-basis: 70%;
`

export const StyledTextFiled = styled(TextField)`
  width: 100%;
  .MuiFormLabel-root.Mui-focused {
    color: #2b6caf;
  }
`

export const StyledButton = styled(Button)`
  &.MuiButton-contained {
    flex-basis: 25%;
    background-color: #2b6caf;
    color: #fff;
    padding: 16px 43px;
    &:hover {
      background-color: #2b6caf;
      color: #fff;
    }
  }
`
