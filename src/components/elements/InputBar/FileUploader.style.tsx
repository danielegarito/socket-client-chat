import styled from "styled-components"
import Grid from "@material-ui/core/Grid"

export const FileUploaderWrapper = styled(Grid)`
  position: absolute;
  top: 0;
  right: 0;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  button {
    height: 100%;
    &.MuiButton-root {
      color: #2b6caf;
    }
  }
`
