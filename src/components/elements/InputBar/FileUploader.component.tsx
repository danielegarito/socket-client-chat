import Button from "@material-ui/core/Button"
import ImageIcon from "@material-ui/icons/Image"
import React from "react"
import { FileUploaderWrapper } from "./FileUploader.style"

interface IFileUploader {
  onUpload: (event: React.ChangeEvent<HTMLInputElement>) => void
}

const FileUploader = ({ onUpload }: IFileUploader) => {
  const hiddenFileInput: React.RefObject<HTMLInputElement> = React.useRef(null)

  const handleClick = () => {
    hiddenFileInput!.current!.click()
  }
  return (
    <FileUploaderWrapper>
      <Button onClick={handleClick}>
        <ImageIcon />
      </Button>
      <input type="file" ref={hiddenFileInput} onChange={onUpload} style={{ display: "none" }} />
    </FileUploaderWrapper>
  )
}

export default FileUploader
