import styled from "styled-components"
import Grid from "@material-ui/core/Grid"

interface IListItem {
  type: string
}

const getBotStyle = (type: string) => {
  if (type !== "bot") return
  return `
    border-radius: 0 15px 15px 20px;
    color: #fff; 
    background-color: #2b6caf;
    border: 0;
  `
}

export const StyledListItemText = styled(Grid)<IListItem>`
  border-radius: 15px 0 20px 15px;
  background-color: #fff;
  border: 1px solid #d3d3d3;
  padding: 5px;
  max-width: 70%;
  span {
    display: flex;
    flex-direction: column;
    &.MuiTypography-caption {
      padding: 0 15px;
    }
  }
  &.MuiListItemText-root {
    margin: 0;
  }
  ${({ type }) => getBotStyle(type)}
`

export const ListItemWrapper = styled(Grid)<IListItem>`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: ${({ type }) => (type !== "bot" ? "flex-end" : "flex-start")};
`
