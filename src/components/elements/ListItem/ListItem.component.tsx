import React from "react"
import Typography from "@material-ui/core/Typography"
import { ListItemWrapper, StyledListItemText } from "./ListItem.style"
import get from "lodash/get"
import { Image } from "../Image/Image.component"
import ListItem from "@material-ui/core/ListItem"
import ListItemText from "@material-ui/core/ListItemText"
import { IMessage } from "../../../store/interfaces"

const ChatListItem = ({ date, text, data, source }: IMessage) => {
  const imgSrc = get(data, "imgSrc")

  return (
    <ListItem>
      <ListItemWrapper type={source}>
        <ListItemText>
          <Typography variant="caption"> {date} </Typography>
        </ListItemText>
        <StyledListItemText type={source}>
          <Image src={imgSrc} text={text} />
          <Typography variant="caption"> {text} </Typography>
        </StyledListItemText>
      </ListItemWrapper>
    </ListItem>
  )
}

export default ChatListItem
